module swgApi

require (
	github.com/didip/tollbooth v4.0.0+incompatible
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8 // indirect
	github.com/go-openapi/analysis v0.17.2 // indirect
	github.com/go-openapi/errors v0.17.2
	github.com/go-openapi/jsonpointer v0.17.2 // indirect
	github.com/go-openapi/jsonreference v0.17.2 // indirect
	github.com/go-openapi/loads v0.17.2
	github.com/go-openapi/runtime v0.17.2
	github.com/go-openapi/spec v0.17.2
	github.com/go-openapi/strfmt v0.17.2
	github.com/go-openapi/swag v0.17.2
	github.com/go-openapi/validate v0.17.2
	github.com/google/uuid v1.1.0 // indirect
	github.com/jessevdk/go-flags v1.4.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/kr/pty v1.1.3 // indirect
	github.com/mailru/easyjson v0.0.0-20180823135443-60711f1a8329
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	golang.org/x/net v0.0.0-20181217023233-e147a9138326
	golang.org/x/time v0.0.0-20181108054448-85acf8d2951c
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
