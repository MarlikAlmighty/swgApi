package myapi

import (
	"net/http"
	"swgApi/models"
	"swgApi/restapi/operations"
	"swgApi/restapi/operations/custom"
	"swgApi/restapi/operations/root"
	"swgApi/restapi/operations/simple"

	"github.com/go-openapi/swag"

	"github.com/go-openapi/runtime/middleware"
)

func ConfigureAPI(api *operations.API) {

	api.RootRootHandler = root.RootHandlerFunc(
		func(
			params root.RootParams,
		) middleware.Responder {
			if len(swag.StringValue(params.Root.Firstname)) != 0 && len(swag.StringValue(params.Root.Lastname)) != 0 {
				return root.NewRootOK().WithPayload(
					&models.Root{
						Firstname: params.Root.Firstname,
						Lastname:  params.Root.Lastname,
					},
				)
			}
			return root.NewRootBadRequest().WithPayload(
				&models.Fail{
					Code:     int32(root.RootBadRequestCode),
					Messages: "No content",
				},
			)
		},
	)

	api.SimpleSimpleHandler = simple.SimpleHandlerFunc(
		func(
			params simple.SimpleParams,
		) middleware.Responder {
			if len(swag.StringValue(params.Simple.Firstname)) != 0 && len(swag.StringValue(params.Simple.Lastname)) != 0 {
				return simple.NewSimpleOK().WithPayload(
					&models.Simple{
						Firstname: params.Simple.Firstname,
						Lastname:  params.Simple.Lastname,
					},
				)
			}
			return simple.NewSimpleBadRequest().WithPayload(
				&models.Fail{
					Code:     int32(simple.SimpleBadRequestCode),
					Messages: "No content",
				},
			)
		},
	)

	api.CustomCustomHandler = custom.CustomHandlerFunc(
		func(
			params custom.CustomParams,
		) middleware.Responder {
			if len(swag.StringValue(params.Custom.Firstname)) != 0 && len(swag.StringValue(params.Custom.Lastname)) != 0 {
				return custom.NewCustomOK().WithPayload(
					&models.Custom{
						Firstname: params.Custom.Firstname,
						Lastname:  params.Custom.Lastname,
					},
				)
			}
			return custom.NewCustomBadRequest().WithPayload(
				&models.Fail{
					Code:     int32(custom.CustomBadRequestCode),
					Messages: "No content",
				},
			)
		},
	)
}

// GlobalMiddleware is a log-enabled HTTP Handler wrapper
type GlobalMiddleware struct {
	Handler http.Handler
}

// ServeHTTP runs an underlaing Handler.
func (m GlobalMiddleware) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	// For logging, tracing...
	m.Handler.ServeHTTP(rw, r)
}
